// my-nodejs-first-public-package.js

console.log("loaded.");
var myNodejsFirstPublicPackage = {};

myNodejsFirstPublicPackage.hello = function() {
    console.log("hello, world");
};

module.exports = myNodejsFirstPublicPackage;
